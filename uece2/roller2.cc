#include <iostream>
#include <vector>

#define foreach(v) for (typeof(v.begin()) it = v.begin(); it != v.end(); ++it)

using namespace std;

int fun[10001];
int cost[10001];
int size[10001];
vector<int> vec[1001];

long long memo[1001][1001];

int l, n, b;

int pd(int pos, int costleft)
{
   if (costleft < 0) return 0;
   if (pos == l) return 2;
   if (pos > l) return 0;
   if (memo[pos][costleft] == -2) return 0;
   if (memo[pos][costleft] != -1) return 1;

//    cout << ":: " << pos << " " << costleft << endl;

   long long bf = -1;

   foreach(vec[pos]) {
      int c = *it;

      int pn = pos+size[c];
      int cn = costleft-cost[c];

      int ret = pd(pn, cn);

//       cout << "-> " << pos << ": " << pn << " " << cn << " " << ret << endl;

      if (!ret) continue;

      long long f = fun[c];
      if (ret == 1) f += memo[pn][cn];

      if (f > bf) bf = f;
   }

   memo[pos][costleft] = bf == -1 ? -2 : bf;

   return bf == -1 ? 0 : 1;
}

int main()
{
   while (cin >> l >> n >> b) {

      memset(memo, -1, 1001*1001*sizeof(long long));

      for (int i=0; i<n; i++) {
         int x;
         cin >> x >> size[i] >> fun[i] >> cost[i];
         vec[x].push_back(i);
      }

      if (pd(0, b)) cout << memo[0][b] << endl;
      else cout << -1 << endl;
   }

   return 0;
}


/*
5 6 10
0 2 20 6
2 3 5 6
0 1 2 1
1 1 1 3
1 2 5 4
3 2 10 2
*/
