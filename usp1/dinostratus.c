#include <stdio.h>
#include <stdlib.h>

int cmp_int (const void *i1, const void *i2)
{
   if (*(int*)i1 > *(int*)i2) return 1;
   else if (*(int*)i1 < *(int*)i2) return -1;
   else return 0;
}

int *gerar_divisores (int n)
{
   int i, j, div[9];

   div[0] = 1;

   for (i = 2, j = 1; i <= n; i++)
  {
      if (j >= 9) return 0;
      if (!(n % i)) div[j++] = i;
   }

//    printf ("Divisores de %d: ", n);
//    for (i = 0; i < j; ++i) printf ("%d ", div[i]);

//    puts ("");

   if (j != 9) return 0;
   int *r = (int*) malloc (9 * sizeof (int));
   for (i = 0; i < 9; ++i) r[i] = div[i];

   return r;
}

/*
int fazer_teste (int num, int *div, int *pos)
{
   int i = 0;

   while (pos[i++] != -1)
      if (num % div[pos]) return 0;

   return 1;
}
*/

int dinostratus (int *div)
{
   int indices[4] = {8, 7, 6, 4};
   int testes[4][3] = {{7, 6, 4}, {5, 4, 2}, {4, 3, 1}, {0, 1, 2}};
   int i, j;

   for (i = 0; i < 4; ++i)
      for (j = 0; j < 3; ++j)
         if (div[indices[i]] % div[testes[i][j]]) return 0;

   return 1;
}

int main ()
{
   int flag, k = 1, n;
   int nd[] =
   {
      36, 225, 256, 441, 1225, 3025, 4225, 5929, 6561, 7225, 8281, 9025, 13225, 14161, 17689, 20449, 25921, 34969, 41209, 43681, 47089, 48841, 61009, 64009, 67081, 82369, 89401, 90601, 101761, 104329, 108241, 116281, 142129, 152881, 162409, 165649, 190969, 203401, 223729, 231361, 243049
   };

   while (scanf ("%d", &n) == 1)
   {
      if (n < 250000)
      {
         int *achou = (int*) bsearch (&n, nd, 41, sizeof (int), &cmp_int);
         flag = (!achou) ? 0 : 1;
         printf ("Instancia %d\n%s\n\n", k, (flag) ? "sim" : "nao");
      }
      else
      {
         int *div = gerar_divisores (n);
         flag = (!div) ? 0 : dinostratus (div);
         printf ("Instancia %d\n%s\n\n", k, (flag) ? "sim" : "nao");
         free (div);
      }
      k++;
   }
   return 0;
}



