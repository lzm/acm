#include <iostream>
#include <vector>

using namespace std;

int k, n, r, best;

int src[10001];
int dst[10001];
int tol[10001];
int len[10001];
int bel[10001];
int bet[10001];

vector<int> city[101];

void dfs(int s, int length, int price)
{
   if (s == n-1) {
      if (length < best)
         best = length;
      return;
   }

   vector<int>::iterator it = city[s].begin();

   for (; it != city[s].end(); ++it) {

      int e = *it;
      int d = dst[e];

      int nel = len[e] + length;
      int net = tol[e] + price;

      if (nel > best) continue;
      if (net > k) continue;

      if (nel >= bel[e] && net >= bet[e]) continue;

      bel[e] = nel;
      bet[e] = net;

      dfs(d, nel, net);
   }
}

int main()
{
   int t;
   scanf("%d", &t);

   while (t--) {
      scanf("%d %d %d", &k, &n, &r);

      for (int i=0; i<n; i++)
         city[i].clear();

      for (int i=0; i<r; i++) {
         scanf("%d %d %d %d", &src[i], &dst[i], &len[i], &tol[i]);
         src[i]--;
         dst[i]--;
         bel[i] = bet[i] = 1 << 30;
         city[src[i]].push_back(i);
      }

      best = 1 << 30;

      dfs(0, 0, 0);

      if (best == 1 << 30) best = -1;

      printf("%d\n", best);
   }

   return 0;
}
