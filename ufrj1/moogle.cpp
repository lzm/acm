#include <iostream>
#include <cmath>

using namespace std;

int h, c;
int ls[201];

float tbl[204][204];

float interp(int s, int f)
{
   float dx = (ls[f]-ls[s])/(f-s);
   float sum = 0;

   for (int i=s+1; i<f; i++)
      sum += fabs(ls[s] + (i-s)*dx - ls[i]);

   return sum;
}

int main()
{
   int t;
   scanf("%d", &t);

   while (t--) {

      scanf("%d%d", &h, &c);

      for (int i=0; i<h; i++)
         scanf("%d", &ls[i]);

      for (int i=0; i<h-1; i++)
         tbl[i][1] = interp(i, h-1);

      for (int k=2; k<c; k++) {
         tbl[h-k-1][k] = 0;

         for (int i=h-k-1; i>=0; i--) {
            float best = -1;

            for (int j=i+1; j<=h-k; j++) {
               float r = interp(i, j) + tbl[j][k-1];
               if (best < 0 || r < best) best = r;
            }

            tbl[i][k] = best;
         }
      }

      printf("%.4f\n", tbl[0][c-1]/h);
   }

   return 0;
}
