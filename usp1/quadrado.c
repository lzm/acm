#include <math.h>
#include <stdio.h>
#include <stdlib.h>

int main()
{
   char mn[1000][10001];
   char mm[60][61];
   int n, m;

   int count = 0;

   while (cin >> n >> m) {

      count++;
      cout << "Instancia " << count << endl;

      for (int i=0; i<n; i++) gets(mn[i]);
      for (int i=0; i<m; i++) gets(mm[i]);

      int k = n-m+1;
      int q = m*m;

      int total = 0;

      for (int j=0; j<k; j++)
      for (int i=0; i<k; i++) {
         int count = 0;

         for (int y=0; y<m; y++)
         for (int x=0; x<m; x++)
            if (mn[j+y] == mm[y]) count++;

         if (count == q) {
            cout << i << " " << j << endl;
         }
      }

      cout << endl;
   }

   return 1;
}
