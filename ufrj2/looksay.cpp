#include <iostream>

using namespace std;

char str[1004];

int main()
{
   int t;
   scanf("%d\n", &t);

   while (t--) {

      scanf("%s", str);

      int len = strlen(str);

      int v = str[0];
      int c = 0;

      for (int i=0; i<=len; i++) {
         if (v == str[i]) c++;
         else {
            printf("%d%c", c, v);
            c = 1;
            v = str[i];
         }
      }
      printf("\n");
   }

   return 0;
}

/*
3
122344111
1111111111
12345
*/
