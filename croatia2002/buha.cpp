// 23:40
// 5 min ler
// 10 codificar
// 5 testar

// 20 total

#include <iostream>

using namespace std;

int map[102][102];

int a, b, k;
int r, s, p, t;

int inside(int i, int j)
{
	int ph = p/2;

	if (	i >= r-ph && i <= r+ph
		&& j >= s-ph && j <= s+ph) return 1;
	return 0;
}

int main()
{
	cin >> a >> b >> k;

	for (int i=1; i<=a; i++) {
		for (int j=1; j<=b; j++) {
			map[i][j] = 1;
		}
	}

	while (k--) {
		cin >> r >> s >> p >> t;
		for (int i=1; i<=a; i++) {
			for (int j=1; j<=b; j++) {
				map[i][j] &= inside(i, j)^(!t);
			}
		}
	}

	int count = 0;
	for (int i=1; i<=a; i++) {
		for (int j=1; j<=b; j++) {
			if (map[i][j]) count++;
		}
	}

	cout << count << endl;

	return 0;
}
