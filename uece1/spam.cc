#include <iostream>

using namespace std;

int lens[26];
char *tbl[] = {"4", "|3", "(", "|)", "3", "|=", "6", "#", "|", "_|",
   "|<", "|_", "|\\/|", "|\\|", "0", "|0", "(,)", "|?", "5", "7",
   "|_|", "\\/", "\\/\\/", "><", "-/", "2"};

char s0[500];
char s[500];
int len;

int memo[500];

int cnt(int p)
{
   if (memo[p] != -1) return memo[p];

   if (p == len) return 1;
   if (p > len) return 0;

   int c = 0;

   for (int i=0; i<26; i++) {
      if (!strncmp(s+p, tbl[i], lens[i])) {
         c += cnt(p+lens[i]);
      }
   }

   memo[p] = c;
   return c;
}

int main()
{
   for (int i=0; i<26; i++) {
      lens[i] = strlen(tbl[i]);
   }

   for (;;) {
      cin >> s0;
      if (!strcmp(s0, "end")) break;

      int l = strlen(s0);
      char *p = s;

      len = 0;

      for (int i=0; i<l; i++) {
         strcpy(s+len, tbl[s0[i]-'A']);
         len += lens[s0[i]-'A'];
      }

      memset(memo, -1, (len+1)*sizeof(int));

      cout << cnt(0) << endl;
   }

   return 0;
}
