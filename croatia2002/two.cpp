// 5 min ler
// 30 min resolver
// 5:30 AM, sono, nao tenho absoluta certeza de que minha solucao ta certa
// 15 min codificar
// 5 min consertar bugs

// 55 total

#include <iostream>
#include <vector>

#define foreach(x) for (typeof(x.begin()) it = x.begin(); it != x.end(); ++it)

using namespace std;

int n, s, a, b, c;

vector< pair<int, int> > g[100002];

int dist[100002], parent[100002], used[100002];
int best, best_n;

int walk(int second)
{
	for (int i=1; i<=n; i++) {
		dist[i] = i==s ? 0 : -1;
		if (!second) {
			used[i] = 0;
		}
	}

	int best = 0;
	int best_n = 0;

	vector<int> next;
	next.push_back(s);

	while (!next.empty()) {
		int i = *(next.end()-1);
		next.pop_back();

		foreach(g[i]) {
			int j = it->first;
			int w = it->second;

			if (dist[j] != -1) continue;

			parent[j] = i;

			if (second && used[j]) w *= -1;

			dist[j] = dist[i] + w;
			if (dist[j] > best) {
				best = dist[j];
				best_n = j;
			}

			next.push_back(j);
		}
	}

	if (!second) {
		int i = best_n;
		while (i != s) {
			used[i] = 1;
			i = parent[i];
		}
	}

	return best;
}

int main()
{
	cin >> n >> s;

	int total = 0;

	for (int i=1; i<n; i++) {
		cin >> a >> b >> c;
		g[a].push_back(make_pair(b, c));
		g[b].push_back(make_pair(a, c));

		total += c;
	}

	int saved1 = walk(0);
	int saved2 = walk(1);

	cout << 2*total-saved1-saved2 << endl;

	return 0;
}
