#include <iostream>
#include <queue>

using namespace std;

long long xs[1001];
long long ys[1001];

int vis[1002];
int lvl[1002];

int n, s, f;
double l1, l2, l;

long long dist(int i, int j)
{
   long long dx = xs[i] - xs[j];
   long long dy = ys[i] - ys[j];

   return dx*dx + dy*dy;
}

int bfs()
{
   queue<int> q;
   q.push(s);

   vis[s] = 1;
   lvl[s] = 1;

   while (!q.empty()) {

      int v = q.front();
      q.pop();

      for (int w=0; w<n; w++) {

         if (vis[w]) continue;
         if (dist(v, w) > l) continue;

         if (w == f) return lvl[v];
         q.push(w);
         vis[w] = 1;
         lvl[w] = lvl[v]+1;
      }
   }

   return -1;
}

int main()
{
   int t;
   cin >> t;

   while (t--) {

      cin >> n >> s >> f >> l1 >> l2;

      l = l1 + l2;
      l = l * l;

      s -= 1;
      f -= 1;

      for (int i=0; i<n; i++) {
         cin >> xs[i] >> ys[i];
         vis[i] = 0;
      }

      int len = bfs();

      if (len == -1) cout << "Impossible\n";
      else cout << len << "\n";
   }

   return 0;
}
