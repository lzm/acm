#include <set>
#include <vector>
#include <iostream>

using namespace std;

const int PC_INF = 100000000; // infinity 


/* Input: adj: an adjacency matrix of the input graph.
 *        st: the starting vertex
 *        ed: the ending vertex
 * Output: the distance of the shortest path from st to ed.
 * Note: the weights are assumed to be integers but can be easily
 *       modifed to real-values.
 */

int t;

int dijkstra(vector< vector<int> > &adj, int st, int ed){
  int size = adj.size(); 
  int d[size]; 
  set< pair<int,int> > pq; 
  
  for(int j=0; j<size; j++) d[j] = PC_INF; 
  d[st] = 0; 
  pq.insert(make_pair(0,st)); 
  
  while(!pq.empty()){
    pair<int,int> cur = *pq.begin(); 
    for(int j=0; j<size; j++) {
      if (j != ed && j != st && j < t) continue;
      if(adj[cur.second][j] + d[cur.second] < d[j]){
	d[j] = adj[cur.second][j] + d[cur.second]; 
	pq.insert(make_pair(d[j],j)); 
      }
    }
    pq.erase(pq.begin()); 
  }  
  return d[ed]; 
}

int main()
{
   int n, m, u, v, w, c, o, d, count = 0;

   vector< vector<int> > adj;

   while (cin >> n >> m)
   {
      count++;
      cout << "Instancia " << count << endl;

      adj.clear();

      for(int k=0; k<n; k++){
         adj.push_back(vector<int>(n, PC_INF));
      }

      for (int i=0; i<m; i++) {
         cin >> u >> v >> w;
         u--;
         v--;
         adj[u][v] = w;
         adj[v][u] = w;
      }

      cin >> c;
      for (int i=0; i<c; i++) {
         cin >> o >> d >> t;

         int r = dijkstra(adj, o-1, d-1);

         cout << (r < PC_INF ? r : -1) << endl;
      }

      cout << endl;
   }

   return 0;
}
