#include <iostream>
#include <cmath>

using namespace std;

int h, c;
int ls[201];

float memo[204][204];
char used[204][204];

float interp(int s, int f)
{
   float dx = ls[f]-ls[s];
   float sum = 0;
   int n = f-s;

   for (int i=s+1; i<f; i++) {
      sum += fabs(ls[s] + (i-s)*dx/n - ls[i]);
   }

   return sum;
}

float err(int p, int n)
{
   int left = h-(p+1);
   int last = h-n;

   if (n == left) return 0;

   if (used[p][n]) return memo[p][n];

   if (n == 1) return interp(p, h-1);

   float best = -1;

   for (int i=p+1; i<=last; i++) {

      float ret = err(i, n-1);
      ret += interp(p, i);

      if (best < 0 || ret < best) {
         best = ret;
      }
   }

   used[p][n] = 1;
   memo[p][n] = best;

   return best;
}

int main()
{
   int t;
   scanf("%d", &t);

   while (t--) {

      scanf("%d%d", &h, &c);

      for (int i=0; i<h; i++) {
         scanf("%d", &ls[i]);
      }

      memset(used, 0, (h+1)*204);

      printf("%.4f\n", err(0, c-1)/h);
   }

   return 0;
}
