#include <iostream>
#include <queue>

#define foreach(v) for (typeof(v.begin()) it = v.begin(); it != v.end(); ++it)

using namespace std;

int vis[100001];
int lvl[100001];
vector<int> vec[100001];
int n, c, nc, c1, c2;

int bfs(int s)
{
   queue<int> q;
   q.push(s);

   vis[s] = 1;
   lvl[s] = 0;

   while (!q.empty()) {

      int v = q.front();
      q.pop();

      foreach(vec[v]) {

         int w = *it;

         if (vis[w] == 0) {
            if (w == c2) return lvl[v];
            q.push(w);
            vis[w] = 1;
            lvl[w] = lvl[v]+1;
         }
      }
   }

   return -1;
}

int main()
{
   int f = 0;

   for (;;) {
      scanf("%d", &n);

      if (n == -1) return 0;

      for (int j=0; j<n; j++) {

         scanf("%d %d", &c, &nc);

         vec[c].clear();
         vis[c] = 0;

         for (int i=0; i<nc; i++) {
            int oc;
            scanf("%d", &oc);
            vec[c].push_back(oc);
         }
      }


      scanf("%d %d", &c1, &c2);

      if (f) printf("\n");
      f = 1;
      printf("%d %d %d\n", c1, c2, bfs(c1));
   }

   return 0;
}
