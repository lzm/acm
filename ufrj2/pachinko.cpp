#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int h, w;

double memo[104][104];
char table[104][104];

double maxwin(int y, int x)
{
   if (y == h) return 0;

   char c = table[y][x];

   if (c == '@') return memo[y][x];

   if (c == '.') {
      table[y][x] = '@';
      memo[y][x] = maxwin(y+1, x);
      return memo[y][x];
   }

   if (c == '*') {
      table[y][x] = '@';
      memo[y][x] = maxwin(y+1, x+1);
      memo[y][x] += maxwin(y+1, x-1);
      memo[y][x] /= 2;
      return memo[y][x];
   }

   return c-'0';
}

int main()
{
   int t;
   scanf("%d\n", &t);

   while (t--) {
      scanf("%d%d\n", &h, &w);

      for (int i=0; i<h; i++) {
         scanf("%s", table[i]);
      }

      double maxw = 0;

      for (int i=0; i<w; i++) {
         double ret = maxwin(0, i);
         if (ret > maxw) maxw = ret;
      }

      printf("%.6f\n", maxw);
   }

   return 0;
}

