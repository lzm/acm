/* UVa 843 - Crypt Kicker
 * http://icpcres.ecs.baylor.edu/onlinejudge/index.php?option=com_onlinejudge&Itemid=8&category=10&page=show_problem&problem=784
 */

#include <iostream>
#include <vector>
#include <string>
#include <map>
#include <set>

#define foreach(x) for (typeof(x.begin()) it = x.begin(); it != x.end(); ++it)
#define tokeach(x) for (char *p = strtok(x, " "); p; p = strtok(0, " "))

using namespace std;

struct cstr {
  bool operator()(char* s1, char* s2) const { return strcmp(s1, s2) < 0; }
};

typedef vector<string>::iterator vit;
map<char*, vector<vit> > can;
vector<string> wl[17];
set<pair<int, char*> > ord;
set<char*, cstr> rep;
char buf[82];
string crack, cracr;
int end;

void rec(set<pair<int, char*> >::iterator pos, string &al, string &ar)
{
   if (pos == ord.end()) {
      crack = al;
      end = 1;
      return;
   }
   string tmp, tmr;
   char *str = pos->second;
   int len = strlen(str);
   pos++;

   foreach(can[str]) {
      tmp = al;
      tmr = ar;
      int err = 0;

      for (int i=0; i<len; ++i) {
         char cw = str[i];
         char cl = (**it)[i];
         char ca = tmp[cw-'a'];

         if (ca != '*' && ca != cl) {
            err = 1;
            break;
         }

         ca = tmr[cl-'a'];

         if (ca != '*' && ca != cw) {
            err = 1;
            break;
         }

         tmp[cw-'a'] = cl;
         tmr[cl-'a'] = cw;
      }
      if (!err) {
         rec(pos, tmp, tmr);
         if (end) return;
      }
   }
}

int psb(char *str)
{
   static char tmp[26];
   int len = strlen(str);
   int n = 0;

   foreach (wl[len]) {
      memset(tmp, 0, 26);
      int err = 0;

      for (int i=0; i<len; i++) {
         char cw = str[i];
         char cl = (*it)[i];
         char ca = tmp[cw-'a'];

         if (ca && ca != cl) {
            err = 1;
            break;
         }
         tmp[cw-'a'] = cl;
      }
      if (!err) {
         n++;
         can[str].push_back(it);
      }
   }
   return n;
}

int main()
{
   int n;
   cin >> n;
   string s;

   for (int i=0; i<n; i++) {
      cin >> s >> ws;
      wl[s.length()].push_back(s);
   }

   while (cin.getline(buf, 82)) {
      s = buf;
      rep.clear();
      ord.clear();
      can.clear();
      end = 0;

      tokeach(buf) rep.insert(p);
      foreach(rep) ord.insert(make_pair(psb(*it), *it));

      crack = "**************************";
      cracr = "**************************";
      rec(ord.begin(), crack, cracr);

      for (int i=0; i<s.length(); i++)
         cout << (s[i] == ' ' ? ' ' : crack[s[i]-'a']);
      cout << endl;
   }

   return 0;
}
