#include <iostream>

using namespace std;

char str[1002];

int grp[] = { 2, 2, 2, 3, 3, 3, 4, 4, 4, 5, 5, 5, 6, 6, 6, 7, 7, 7, 7, 8, 8, 8, 9, 9, 9, 9 };
int pos[] = { 1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3, 1, 2, 3, 4, 1, 2, 3, 1, 2, 3, 4 };

int group(int i)
{
   if (str[i] == ' ') return 1;
   return grp[str[i]-'A'];
}

int cost(int i)
{
   if (str[i] == ' ') return 1;
   return pos[str[i]-'A'];
}

int main()
{
   int t;

   scanf("%d", &t);

   while (t--) {

      int p, w;
      cin >> p >> w >> ws;

      cin.getline(str, 1001);

      int len = strlen(str);
      int sum = cost(0) * p;

      for (int i=1; i<len; i++) {
         if (group(i) != 1 && group(i) == group(i-1)) sum += w;
         sum += cost(i) * p;
      }

      cout << sum << endl;
   }

   return 0;
}
