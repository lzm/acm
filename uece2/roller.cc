#include <algorithm>
#include <iostream>
#include <vector>

#define foreach(v) for (typeof(v.begin()) it = v.begin(); it != v.end(); ++it)

using namespace std;

int ls[10001];
int fs[10001];
int cs[10001];

vector<int> vs[1001];

long long t[1001][1001]; // position, cost
char u[1001][1001];

int l, n, b;

long long pd(int pos, int budget)
{
   if (pos == l) return 0;
   if (u[pos][budget]) return t[pos][budget];

   long long ret, ff = -1;

   foreach(vs[pos]) {
      int i = *it;

      if (budget < cs[i]) continue;
      if (l < pos+ls[i]) continue;

      ret = pd(pos+ls[i], budget-cs[i]);
      if (ret == -1) continue;

      ff = max(ff, ret+fs[i]);
   }

   u[pos][budget] = 1;
   return t[pos][budget] = ff;
}

int main()
{
   int x;
   while (cin >> l >> n >> b) {

      for (int i=0; i<n; i++) {
         cin >> x >> ls[i] >> fs[i] >> cs[i];
         for (int j=x; j<=l-ls[i]; j++)
            vs[j].push_back(i);
      }

      memset(u, 0, 1001*1001*sizeof(char));

      cout << pd(0, b) << endl;
   }

   return 0;
}

/*
5 6 10
0 2 20 6
2 3 5 6
0 1 2 1
1 1 1 3
1 2 5 4
3 2 10 2
*/
