#include <iostream>
#include <string>
#include <set>

#define chk(a,b,c) (s[a] == s[b] && s[a] == s[c])

using namespace std;

int check(const string &s)
{
   for (int i=0; i<3; i++) {
      if (chk(i, i+3, i+6)) return 1;
      if (chk(i*3, i*3+1, i*3+2)) return 1;
   }

   if (chk(0, 4, 8)) return 1;
   if (chk(2, 4, 6)) return 1;

   return 0;
}

set<string> all;

void vai(string state, char vez)
{
   int foi = 0;

   //if (all.find(state) != all.end()) return;

   for (int i=0; i<9; i++) {
      if (state[i] == '0'+i) {
         foi = 1;
         state[i] = vez;

         if (check(state)) {
            //cout << state << endl;
            all.insert(state);
         } else {
            vai(state, vez == 'X' ? 'O' : 'X');
         }
         state[i] = '0'+i;
      }
   }

   if (!foi) {
      //cout << state << endl;
      all.insert(state);
   }
}

int main()
{
   string sta = "012345678";

   vai(sta, 'X');

   string s;

   for (;;) {

      cin >> s;
      if (s == "end") break;

      for (int i=0; i<9; i++) {
         if (s[i] == '.') s[i] = '0'+i;
      }

      if (all.find(s) == all.end()) {
         cout << "invalid" << endl;
      } else {
         cout << "valid" << endl;
      }
   }

   return 0;
}
/*
XXXOO.XXX
XOXOXOXOX
OXOXOXOXO
XXOOOXXOX
XO.OX...X
.XXX.XOOO
X.OO..X..
OOXXXOOXO
end
*/
