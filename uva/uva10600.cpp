// uva 10600: second best mst, prim

#include <iostream>
#include <vector>
#include <set>
#include <cstring>

#define foreach(x) for(typeof(x.begin()) it = x.begin(); it != x.end(); ++it)

using namespace std;

int n, m;
vector< vector< pair<int, int> > > G, T;
int in_tree[100][100];

int prim()
{
    set< pair<int, int> > Q;
    vector<int> key(n, 999999999), parent(n), visited(n);

    key[0] = 0;

    memset(in_tree, 0, sizeof(in_tree));

    for (int i=0; i<n; i++)
        Q.insert(make_pair(key[i], i));

    while (!Q.empty()) {
        int u = Q.begin()->second;
        Q.erase(Q.begin());
        visited[u] = true;
        if (u) {
            int p = parent[u];
            T[u].push_back(make_pair(p, key[u]));
            T[p].push_back(make_pair(u, key[u]));
            in_tree[u][p] = in_tree[p][u] = 1;
        }

        foreach (G[u]) {
            int v = it->first;
            int c = it->second;
            if (!visited[v] && c < key[v]) {
                Q.erase(Q.find(make_pair(key[v], v)));
                Q.insert(make_pair(c, v));
                parent[v] = u;
                key[v] = c;
            }
        }
    }

    int cost = 0;
    for (int i=0; i<n; i++)
        cost += key[i];

    return cost;
}

int second()
{
    // pra todos os pares (u,v), ache a maior aresta do caminho u-v na MST.

    vector< vector<int> > max_(n);

    for (int u=0; u<n; u++) {
        max_[u].assign(n, -1);
        set<int> Q;
        Q.insert(u);

        while (!Q.empty()) {
            int x = *Q.begin();
            Q.erase(Q.begin());

            foreach (T[x]) {
                int v = it->first;
                int c = it->second;
                if (max_[u][v] == -1 && v != u) {
                    if (x == u || c > max_[u][x]) {
                        max_[u][v] = c;
                    } else {
                        max_[u][v] = max_[u][x];
                    }
                    Q.insert(v);
                }
            }
        }
    }

    // ve qual aresta fora da arvore eh a melhor

    int min_diff = 999999999;

    for (int u=0; u<n; u++) {
        foreach (G[u]) {
            int v = it->first;
            int c = it->second;

            if (in_tree[u][v]) continue;

            int diff = c - max_[u][v];
            if (diff < min_diff)
                min_diff = diff;
        }
    }

    return min_diff;
}

int main()
{
    int t;
    cin >> t;

    while (t--) {
        cin >> n >> m;

        G.clear();
        G.resize(n);
        T.clear();
        T.resize(n);

        for (int i=0; i<m; i++) {
            int u, v, c;
            cin >> u >> v >> c;  // numerados de 1 a n

            if (u == v) continue;

            G[u-1].push_back(make_pair(v-1, c));
            G[v-1].push_back(make_pair(u-1, c));
        }

        int p = prim();
        int d = second();

        cout << p << " " << p+d << endl;
    }

    return 0;
}

