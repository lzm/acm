// uva 348: optimal matrix multiplication
// pd, memo, pretty print

#include <iostream>
#include <string>
#include <cstring>

using namespace std;

int r[10];
int c[10];
int cut[10][10];
int memo[10][10];

int mults(int a, int b)
{
    if (memo[a][b] != -1)
        return memo[a][b];

    if (a == b)
        return 0;

    int best_m = -1;
    int best_i = -1;

    for (int i=a; i<b; i++) {
        int m = r[a]*c[i]*c[b] + mults(a, i) + mults(i+1, b);
        if (m < best_m || best_m == -1) {
            best_m = m;
            best_i = i;
        }
    }

    cut[a][b] = best_i;
    memo[a][b] = best_m;
    return best_m;
}

char buf[10];

string show(int a, int b)
{
    if (a == b) {
        sprintf(buf, "A%d", a+1);
        return string(buf);
    }

    string s = "(";
    s += show(a, cut[a][b]);
    s += " x ";
    s += show(cut[a][b]+1, b);
    s += ")";
    return s;
}

int main()
{
    int n, k=1;

    while (1) {
        cin >> n;
        if (!n) break;

        memset(cut, -1, sizeof(cut));
        memset(memo, -1, sizeof(memo));

        for (int i=0; i<n; i++) {
            cin >> r[i] >> c[i];
        }

        mults(0, n-1);

        cout << "Case " << (k++) << ": " << show(0, n-1) << endl;
    }

    return 0;
}

