#include <iostream>
#include <algorithm>

using namespace std;

int main()
{
   int lx, ly, lz, x, y, z;

   for (;;) {
      cin >> lx >> ly >> lz >> x >> y >> z;

      if (!lx && !ly && !lz && !x && !y && !z) return 0;

      if (x == 0 || y == 0 || z == 0) {
         cout << x*x + y*y + z*z << endl;
         continue;
      }

      int dist = 0;

      //if (y == ly) {
         dist = (ly+x)*(ly+x) + z*z;
         dist = min(dist,(ly+z)*(ly+z) + x*x);
      //}

      //if (x == lx) {
         dist = min(dist,(lx+y)*(lx+y) + z*z);
         dist = min(dist,(lx+z)*(lx+z) + y*y);
      //}
      //if (z == lz) {
         dist = min(dist,(lz+y)*(lz+y) + x*x);
         dist = min(dist,(lz+x)*(lz+x) + y*y);
      //}

      cout << dist << endl;
   }

   return 0;
}
