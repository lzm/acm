// 10 min ler
// 10 min resolver
// 20 min codificar
// 13 min consertar bugs
// 5 min dfs() -> walk(), medinho de stack overflow

// 58 total

#include <iostream>
#include <vector>

#define foreach(x) for (typeof(x.begin()) it = x.begin(); it != x.end(); ++it)

using namespace std;

int n, s, a, b, c;

vector< pair<int, int> > g[100002];
int dist[100002], best;
vector<int> next;

void walk()
{
	while (!next.empty()) {
		int i = *(next.end()-1);
		next.pop_back();

		foreach(g[i]) {
			int j = it->first;
			int w = it->second;

			if (dist[j] || j == s) continue;

			dist[j] = dist[i] + w;
			if (dist[j] > best)
				best = dist[j];

			next.push_back(j);
		}
	}
}

int main()
{
	cin >> n >> s;

	int total = 0;

	for (int i=1; i<n; i++) {
		cin >> a >> b >> c;
		g[a].push_back(make_pair(b, c));
		g[b].push_back(make_pair(a, c));

		total += c;
	}

	for (int i=1; i<=n; i++) {
		dist[i] = 0;
	}

	best = 0;
	next.push_back(s);
	walk();

	cout << 2*total-best << endl;

	return 0;
}
