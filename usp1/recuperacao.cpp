#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <inttypes.h>
#include <ctype.h>
#include <algorithm>
#include <utility>
#include <iostream>

using namespace std;

int main()
{
   int n, count = 0;
   int soma;
   int num;
   int achou;

   while (scanf("%d", &n) == 1)
   {
      count++;

      soma = 0;
      achou = 0;

      for (int i=0; i<n; i++) {
         scanf("%d", &num);

         if (soma == num) {
            if (!achou) {
               cout << "Instancia " << count << endl;
               cout << num << endl << endl;
               achou = 1;
            }
         }
         soma += num;
      }

      if (!achou) {
         cout << "Instancia " << count << endl;
         cout << "nao achei" << endl << endl;
      }
   }

   return 0;
}
