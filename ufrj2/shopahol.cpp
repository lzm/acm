#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int main()
{
   int t;
   cin >> t;

   while (t--) {
      int n;
      cin >> n;

      vector<int> vec;

      for (int i=0; i<n; i++) {
         int num;
         cin >> num;
         vec.push_back(num);
      }

      sort(vec.begin(), vec.end());

      reverse(vec.begin(), vec.end());

      int sum = 0;

      for (int i=2; i<n; i+=3)
         sum += vec[i];

      cout << sum << endl;
   }

   return 0;
}
