#include <iostream>
#include <set>

using namespace std;

int used[1000];
int size = 1000;

#define mpp(x,y,z) make_pair(x,make_pair(y,z))
#define pp(x,y,z) pair<x,pair<y,z> > 

#define pp0(p) ((p).first)
#define pp1(p) (((p).second).first)
#define pp2(p) (((p).second).second)

int main()
{
   int n, m, x, y, c;
   int count = 0;

   set< pp(int, int, int) > rest;

   while (cin >> n)
   {
      cin >> m;
      count++;
      cout << "Instancia " << count << endl;

      rest.clear();
      memset(used, 0, sizeof(used));

      for (int i=0; i<m; i++) {
         cin >> x >> y >> c;
         x--;
         y--;

         rest.insert( mpp(c, x, y) );
         rest.insert( mpp(c, y, x) );
      }

      int first = 1;
      int total = 0;

      while (!rest.empty()) {

         pp(int,int,int) cur = *rest.begin();

         //cout << "testing " << pp0(cur) << " " << pp1(cur) << " " << pp2(cur) << endl;

         if (first) {
            first = 0;
            used[pp1(cur)] = 1;
         }

         if (used[pp1(cur)] && !used[pp2(cur)])
         {
            used[pp2(cur)] = 1;

            //cout << "adding --------- " << pp0(cur) << endl;
            total += pp0(cur);
         }
         //else cout << "discarding" << endl;

         if (used[pp1(cur)] || used[pp2(cur)])
            rest.erase(rest.begin());
      }

      cout << total << endl;
      cout << endl;
   }

   return 0;
}
