#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <inttypes.h>
#include <ctype.h>
#include <algorithm>
#include <utility>
#include <iostream>

using namespace std;

#define TRACE(x...)
#define PRINT(x...) TRACE(printf(x))
#define WATCH(x) TRACE(cout << #x" = " << x << "\n")

#define foreach(it, b, e) for (typeof(b) it = (b); it != (e); it++)
#define foreach2(x) for (typeof((x).begin()) it = (x).begin(); it != (x).end(); it++)

int main()
{
   int n, count = 0;


   while (cin >> n)
   {
      count++;
      int menornota = -1;
      string pior;

      for (int i=0; i<n; i++) {
         string nome;
         int nota;

         cin >> nome >> nota;

         if (menornota == -1 || nota <= menornota) {

            if (menornota == -1 || nota < menornota || nome > pior) {
               pior = nome;
            }

            menornota = nota;
         }
      }

      cout << "Instancia " << count << endl;
      cout << pior << endl << endl;
   }

   return 0;
}
