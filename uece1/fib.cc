#include <iostream>

using namespace std;

int main()
{
   int n, a, b, c;

   for (;;) {
      cin >> n;
      if (n == -1) return 0;

      a = 0;
      b = 1;
      c = 0;

      n = n % 15000;

      for (int i=0; i<n; i++) {
         c = a + b;
         a = b;
         b = c % 10000;
      }

      cout << a << endl;
   }

   return 0;
}
