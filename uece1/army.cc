#include <iostream>
#include <set>

#define foreach(x) for (typeof(x.begin()) it = x.begin(); it != x.end(); ++it)

using namespace std;

int memo[1001];
int as[1001];
int r, n;

int pd(int i)
{
   if (memo[i] != -1) return memo[i];

   if (i == n) return 0;

   int best = 999999;
   int p = i;

   while (p < n && as[i] >= as[p]-r) {

      int f = p;
      while (f < n && as[f] <= as[p]+r)
         f++;

      int ret = 1 + pd(f);

      if (ret < best) best = ret;

      p++;
   }

   memo[i] = best;
   return best;
}

int main()
{
   int x;

   for (;;) {
      cin >> r >> n;

      if (n == -1) return 0;

      memset(memo, -1, (n+1)*sizeof(int));

      set<int> s;

      for (int i=0; i<n; i++) {
         cin >> x;
         s.insert(x);
      }

      n = 0;

      foreach (s) {
         as[n] = *it;
         n++;
      }

      cout << pd(0) << endl;
   }

   return 0;
}
