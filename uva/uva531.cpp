#include <iostream>
#include <string>
#include <vector>
#include <cstring>

using namespace std;

vector<string> a, b;
string word;

int memo[100][100];

int lcs(int i, int j)
{
    if (i<0 || j<0)
        return 0;

    if (memo[i][j] != -1)
        return memo[i][j];

    if (a[i] == b[j])
        memo[i][j] = 1+lcs(i-1, j-1);
    else
        memo[i][j] = max(lcs(i-1, j), lcs(i, j-1));

    return memo[i][j];
}

string show(int i, int j)
{
    if (i<0 || j<0)
        return "";

    if (a[i] == b[j])
        return show(i-1, j-1) + " " + a[i];

    if (lcs(i, j-1) > lcs(i-1, j))
        return show(i, j-1);
    else
        return show(i-1, j);
}

int main()
{
    while (1) {
        a.clear();
        b.clear();
        memset(memo, -1, sizeof(memo));

        while (1) {
            if (!(cin >> word)) return 0;
            if (word == "#") break;
            a.push_back(word);
        }

        while (1) {
            cin >> word;
            if (word == "#") break;
            b.push_back(word);
        }

        cout << (show(a.size()-1, b.size()-1).c_str()+1) << endl;
    }
    return 0;
}

