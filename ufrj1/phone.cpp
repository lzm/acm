#include <iostream>

using namespace std;

char str[10001][11];
char *vec[10001];

int cmp(const void *s1, const void *s2)
{
   char *str1 = *(char**)s1;
   char *str2 = *(char**)s2;

   return strcmp(str1, str2);
}

int prefix(char *a, char *b)
{
   int la = strlen(a);
   int lb = strlen(b);

   if (la > lb) return 0;

   return !strncmp(a, b, la);
}

int main()
{
   int t;

   cin >> t;

   while (t--) {

      int n;
      scanf("%d", &n);

      for (int i=0; i<n; i++) {
         scanf("%s", str[i]);
         vec[i] = str[i];
      }

      qsort(vec, n, sizeof(char*), cmp);

      int c = 0;

      for (int i=1; i<n; i++) {
         if (prefix(vec[i-1], vec[i])) {
            c = 1;
            break;
         }
      }

      if (c) printf("NO\n");
      else printf("YES\n");
   }

   return 0;
}
