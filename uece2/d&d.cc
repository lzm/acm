#include <iostream>

using namespace std;

int d, x;
int ds[13];

long long memo[13][1001];

long long num(int di, int left)
{
   if (di == d) return left == 0;
   if (left < 1) return 0;
   if (memo[di][left] != -1) return memo[di][left];

   long long n = 0;

   for (int i=1; i<=ds[di]; i++)
      n += num(di+1, left-i);

   memo[di][left] = n;
   return n;
}

char str[10];

int main()
{
   for (;;) {
      cin >> d;
      if (d == 0) return 0;

      memset(memo, -1, 13*1001*sizeof(long long));

      long long m = 1;

      for (int i=0; i<d; i++) {
         cin >> str;
         ds[i] = atoi(str+1);
         //cout << ds[i] << endl;
         m *= ds[i];
      }

      //cout << m << endl;

      cin >> x;

      //long long n = num(0, x);

      //cout << n << endl; 

      double f = num(0, x);
      f /= m;

      printf("%.5f\n", f);
   }

   return 0;
}

/*
1 d10 5
2 d6 d6 1
2 d10 d6 9
2 d8 d8 9
0
*/
