// 5 min ler
// 15 min se convencer que eh distancia de edicao
// 10 min codificar
// 5 min bugs

// 35 total

#include <iostream>
#include <cstring>

using namespace std;

char a[2002], b[2002];
int c[2002][2002], k;

int abs(int x)
{
	return x<0?-x:x;
}

int main()
{
	cin >> a;
	cin >> b;
	cin >> k;

	int sa = strlen(a);
	int sb = strlen(b);

	for (int i=0; i<=sa; i++) {
		c[i][0] = i*k;
	}

	for (int j=0; j<=sb; j++) {
		c[0][j] = j*k;
	}

	for (int i=1; i<=sa; i++) {
		for (int j=1; j<=sb; j++) {
			int c1 = c[i-1][j-1] + abs(a[i-1]-b[j-1]);
			int c2 = c[i-1][j] + k;
			int c3 = c[i][j-1] + k;

			if (c2 < c1)
				c1 = c2;
			if (c3 < c1)
				c1 = c3;

			c[i][j] = c1;
		}
	}

	cout << c[sa][sb] << endl;

	return 0;
}
