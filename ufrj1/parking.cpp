#include <iostream>

using namespace std;

int main()
{
   int t;

   scanf("%d", &t);

   while (t--) {

      int n;
      scanf("%d", &n);

      int menor = -1;
      int maior = -1;

      for (int i=0; i<n; i++) {
         int pos;
         scanf("%d", &pos);
         if (menor == -1 || menor > pos)
            menor = pos;

         if (maior == -1 || maior < pos)
            maior = pos;
      }

      int dst = maior - menor;

      printf("%d\n", 2*dst);
   }

   return 0;
}
