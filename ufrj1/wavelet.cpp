#include <iostream>

using namespace std;

int veca[256];
int vecb[256];

int *end;
int *src;
int *dst;

int n;

void tr(int t)
{
   if (t > n) {
      end = src;
      return;
   }

   int h = t >> 1;

   for (int i=0; i<h; i++) {

      int s = src[i];
      int d = src[i+h];

      int a = (s + d) / 2;
      int b = s - a;

      dst[i*2] = a;
      dst[i*2+1] = b;
   }

   end = src;
   src = dst;
   dst = end;

   tr(t*2);
}

int main()
{
   for (;;) {

      scanf("%d", &n);
      if (n == 0) return 0;

      for (int i=0; i<n; i++) {
         scanf("%d", &veca[i]);
         vecb[i] = veca[i];
      }

      src = veca;
      dst = vecb;

      if (n > 1) tr(2);

      printf("%d", end[0]);

      for (int i=1; i<n; i++) {
         printf(" %d", end[i]);
      }
      printf("\n");
   }

   return 0;
}
