#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <inttypes.h>
#include <ctype.h>
#include <algorithm>
#include <utility>
#include <iostream>
#include <vector>

#define foreach(it, b, e) for (typeof(b) it = (b); it != (e); it++)
#define foreach2(x) for (typeof((x).begin()) it = (x).begin(); it != (x).end(); it++)

using namespace std;

int cor[100];
vector<int> vec[100];

int n, m;
int pode, x, y;

set<int> rest;

int testa(int x, int c)
{
   cor[x] = c;

   foreach2(vec[x]) {
      int i = *it;
      if (cor[i]) {
         if (cor[i] == c) return 0;
      } else {
         if (!testa(i, -c)) {
            return 0;
         }
      }
   }
   return 1;
}

int podef()
{
   for (int i=0; i<n; i++) {
      if (!cor[i]) {
         if (!testa(i, 1)) return 0;
      }
   }

   return 1;
}

int main()
{
   int count = 0;

   while (cin >> n >> m)
   {
      count++;

      memset(cor, 0, n*sizeof(int));

      for (int i=0; i<n; i++)
         vec[i].clear();

      for (int i=0; i<m; i++) {
         cin >> x >> y;
         x--;
         y--;

         if (x > y) {
            swap(x, y);
         }

         vec[x].push_back(y);
      }

      cout << "Instancia " << count << endl;
      cout << (podef() ? "sim" : "nao") << endl << endl;
   }

   return 0;
}
