#include <iostream>
#include <map>

using namespace std;

int hash[10004];
int as[10004];
int bs[10004];

map<int, int> rehash;

int m, n;

int reloc(int i)
{
   if (rehash[i] == m) return 0;

   if (hash[as[i]] == -1) {
      hash[as[i]] = i;
      return 1;
   }
   if (hash[bs[i]] == -1) {
      hash[bs[i]] = i;
      return 1;
   }

   rehash[i] = rehash[i] + 1;

   int j = hash[as[i]];
   hash[as[i]] = i;
   return reloc(j);
}

int main()
{
   int t;
   scanf("%d", &t);

   while (t--) {

      scanf("%d %d", &m, &n);

      memset(hash, -1, sizeof(int)*m);

      int pos = 1;

      for (int i=0; i<m; i++) {
         scanf("%d %d", &as[i], &bs[i]);
         rehash.clear();
         pos &= reloc(i);
      }

      if (pos) puts("successful hashing");
      else puts("rehash necessary");
   }

   return 0;
}
