#include <iostream>

using namespace std;

unsigned int divs[] = { 486486000, 54054000, 5405400, 491400, 40950, 3150, 225, 15, 1, 0 };
unsigned int tbl[20];
char hexs[20];
char vec[8];

int main()
{
   int t = 1000;
   cin >> t;

   while (t--) {
      unsigned int n;
      cin >> n;

      strcpy(hexs, "FEDCBA9876543210");
      strcpy(vec, "00000000");

      int digits = 8;

      if (n > 546481140) {
         cout << 0 << endl;
         continue;
      }

      for (int k=0; k<8; k++) {
         if (n <= divs[k]) break;
         digits--;
         n -= divs[k];
      }

      n--;

      for (int i=0; i<digits; i++) tbl[i] = 16-i;
      for (int i=digits-2; i>=0; i--) tbl[i] *= tbl[i+1];
      tbl[digits] = 1;

      for (int i=0; i<digits; i++) {

         unsigned int q = n / tbl[i+1];
         n = n % tbl[i+1];

         int j = 0;
         while (j<16) {
            if (hexs[j] != '.') {
               if (!q) break;
               q--;
            }
            j++;
         }

         vec[i] = hexs[j];
         hexs[j] = '.';
      }

      vec[digits] = 0;
      cout << vec << endl;
   }

   return 0;
}
