#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <inttypes.h>
#include <ctype.h>
#include <algorithm>
#include <utility>
#include <iostream>

using namespace std;

int vec[1000000];

int main()
{
   long long int fat;

   fat = 1;

   for (long long int i=1; i<=1000000; i++) {

      fat *= i;

      while ((fat % 10) == 0) {
         fat /= 10;
      }

      fat = fat % 10000000;

      vec[i-1] = fat;
   }

   int n, count = 0;

   while (cin >> n)
   {
      count++;
      fat = vec[n-1] % 10;
      cout << "Instancia " << count << endl;
      cout << fat << endl << endl;
   }

   return 0;
}
