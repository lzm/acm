// 3 min ler
// 15 min tentar resolver
// 30 min codificar e tentar resolver mais
// 0 min corrigir bug

// 48 total

#include <iostream>

using namespace std;

int n, m;
int a[100002];
int b[100002];
int b0[100002]; // start
int b1[100002]; // end
int used[100002];
int main()
{
	cin >> n >> m;

	b[0] = 1;
	for (int i=1; i<=n; i++) {
		used[i] = 0;
		a[i] = 0;
		cin >> b[i];

		if (!b[i-1]) {
			b0[i] = b0[i-1];
		} else {
			b0[i] = i;
		}

		if (b[i]) {
			for (int j=b0[i]; j<=i; j++) {
				b1[j] = i;
			}
		}
	}

	if (b[n] == 0) {
		cout << "not exist" << endl;
		return 0;
	}

	int fail = 0;

	for (int i=1; i<=m; i++) {
		int x, y;
		cin >> x >> y;

		if (y < b0[x] || y > b1[x]) {
			fail = 1;
		}

		used[y] = 1;
		a[x] = y;
	}

	if (fail) {
		cout << "not exist" << endl;
		return 0;
	}

	int pos = 1;
	int num = 1;

	for (;;) {
		if (used[num]) {
			num++;
			continue;
		}

		if (a[pos]) {
			pos++;
			continue;
		}

		if (num > n) break;

		a[pos++] = num++;
	}

	cout << a[1];
	for (int i=2; i<=n; i++) {
		cout << " " << a[i];
	}
	cout << endl;

	return 0;
}
