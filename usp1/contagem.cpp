#include <algorithm>
#include <utility>
#include <iostream>
#include <map>

using namespace std;

static map<pair<int,int>, int> memo;

int changes(int n, int v)
{
   if (v == -1) return 0;
   if (n < 0) return 0;
   if (n == 0) return 1;

   if (memo.find(make_pair(n, v)) != memo.end())
      return memo[make_pair(n, v)];

   if (vals[v] <= n)
      memo[make_pair(n, v)] = changes(n-vals[v], v);

   return memo[make_pair(n, v)] += changes(n, v-1);
}

int main_coin()
{
   char str[62];

   while (cin >> str)
      cout << changes(n, 4) << endl;

   return 0;
}
