#include <iostream>

using namespace std;

int map(char c)
{
   if (c >= '0' && c <= '9') return c - '0';
   if (c >= 'A' && c <= 'Z') return c - 'A' + 10;
   return c - 'z' + 61;
}

char str[100001];

int main()
{
   for (;;) {
      scanf("%s", str);
      if (!strcmp(str, "end")) return 0;

      int l = strlen(str);
      int sum = 0;

      for (int i=0; i<l; i++) {
         sum += map(str[i]);
      }

      if (sum % 61 == 0)
         puts("yes");
      else
         puts("no");
   }

   return 0;
}
