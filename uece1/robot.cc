#include <iostream>
#include <cmath>
#include <vector>

#define foreach(x) for (typeof(x.begin()) it = x.begin(); it != x.end(); ++it)

using namespace std;

int xs[20];
int ys[20];
int r, n;
int best;

float calc(vector<int> falta, float tot, float ang)
{
   cout << "falta ";
   for (int j=0; j<falta.size(); j++) {
      cout << falta[j] << " ";
   }
   cout << endl;

   foreach (falta) {
      int i = *it;

      float targ = atan2(ys[i]-ys[0], xs[i]-xs[0]);
      float a = fabs(targ - ang);

      cout << "gabs " << a << endl;

      if (a >= M_PI) a = 2*M_PI - a;

      a = a * 180.0 / M_PI;

      int dx = xs[i]-xs[0];
      int dy = ys[i]-ys[0];

      float dist = sqrtf(dx*dx+dy*dy);

      cout << "dst a " << dist << " " << a << endl;

      float t = dist + a;

      float tot2 = tot + t;

      if (round(t) > r) continue;

      if (i == n-1) {
         if (tot2 < best) best = round(tot2);
         continue;
      }

      vector<int> falta2;
      int len = falta.size();

      for (int j=0; j<len; j++) {
         cout << "i = " << i << " p " << j << endl;
         if (i != falta[j]) falta2.push_back(falta[j]);
      }

      calc(falta2, tot2, targ);
   }
}

int main()
{
   for (;;) {
      cin >> r >> n;

      for (int i=0; i<n; i++) {
         cin >> xs[i] >> ys[i];
      }

      vector<int> falta;

      for (int i=1; i<n; i++) {
         falta.push_back(i);
      }

      best = 999999;

      float ang = atan2(ys[n-1]-ys[0], xs[n-1]-xs[0]);

      calc(falta, 0, ang);

      if (best == 999999) cout << "impossible" << endl;
      else cout << "bet " << best << endl;
   }

   return 0;
}

/*
10 2
0 0
7 0

10 3
0 0
7 0
14 5

10 3
0 0
7 0
14 10
-1 -1
*/